import React from 'react';

import { PostProvider } from './PostContext';

const Store = ({ children }) => (
  <PostProvider>
    {children}
  </PostProvider>
);

export default Store;
