import React, {createContext, useState} from 'react';

const PostContext = createContext([{}, () => {}])

const PostProvider = (props) => {
      const [posts, setPosts] = useState([])

      const update = () => fetch('https://jsonplaceholder.typicode.com/posts')
      .then(response => response.json())
      .then(json => setPosts(json))

      return (
        <PostContext.Provider value={{posts, update}}>
          {props.children}
        </PostContext.Provider>
      );
}


export {PostContext, PostProvider};