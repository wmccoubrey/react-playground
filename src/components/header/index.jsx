import React from 'react';

import {
  Button,
  ButtonGroup,
  Container, Dropdown, Image, Menu,
} from 'semantic-ui-react';
import logo from './logo.svg';

const Header = () => (
  <Menu fixed="top" inverted>
    <Container>
      <Menu.Item as="a" header>
        <Image size="mini" src={logo} style={{ marginRight: '1.5em' }} />
                React Context & Hooks
      </Menu.Item>

      <Menu.Item as="a">Home</Menu.Item>

      <Menu.Item as="a">Posts</Menu.Item>

      <Menu.Item as="a">Products</Menu.Item>

      <Menu.Menu position="right">
        <Menu.Item>
          <ButtonGroup>
            <Button primary inverted>Add Post</Button>
            <Button primary inverted>Add Product</Button>
          </ButtonGroup>
        </Menu.Item>
      </Menu.Menu>
    </Container>
  </Menu>
);

export default Header;
