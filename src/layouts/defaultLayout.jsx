import React from 'react';

import { Container } from 'semantic-ui-react';
import Header from '../components/header';

const DefaultLayout = ({ children }) => (
  <div>
    <Header />

    <Container text style={{ marginTop: '7em' }}>
      {children}
    </Container>
  </div>
);

export default DefaultLayout;
