import queryString from 'query-string';
import axios from 'axios';

const API_URL = 'http://localhost:3001';
const ENDPOINT_POSTS = '/posts';

const getRequest = (endpoint, params = {}) => axios.get(`${API_URL}${endpoint}?${queryString.stringify(params)}`);

const postRequest = (endpoint, body = {}) => axios.post(`${API_URL}${endpoint}`, body);

export const getPosts = (...args) => getRequest(ENDPOINT_POSTS, ...args);
export const getPost = (id, ...args) => getRequest(`${ENDPOINT_POSTS}/${id}`, ...args);
export const createPost = (post) => postRequest(ENDPOINT_POSTS, post);
export const updatePost = ({ id, ...post }) => postRequest(`${ENDPOINT_POSTS}/${id}`, post);
